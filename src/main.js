import Vue from 'vue';
import Antd from 'vue-antd-ui';
import VueResource from 'vue-resource';
import VueLocalStorage from 'vue-localstorage';

import App from './App';
import { router } from './router';
import { store } from './store';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'vue-antd-ui/dist/antd.css';

Vue.config.productionTip = false;

Vue.use(VueResource);
Vue.use(VueLocalStorage);
Vue.use(Antd);

Vue.http.interceptors.push(function(request) {
  request.headers.set('Authorization', 'Bearer '+Vue.localStorage.get('access_token'));
});

/* eslint-disable no-new */

export const VmEvents = new Vue();

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});