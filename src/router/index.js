import Vue from 'vue';
import Router from 'vue-router';
import { store } from '@/store';

import IndexLayout from '@/layouts/IndexLayout';
import DashboardLayout from '@/layouts/DashboardLayout';
import TasksLayout from '@/layouts/TasksLayout';

import LoginPage from '@/pages/LoginPage';
import UsersPage from '@/pages/UsersPage';

import TasksPage from '@/pages/tasks/TasksPage';
import currentTasksPage from '@/pages/tasks/currentTasksPage';
import newTaskPage from '@/pages/tasks/newTaskPage';

import DevicesPage from '@/pages/DevicesPage';
import TicketsPage from '@/pages/TicketsPage';
import newDepartmentPage from '@/pages/newDepartmentPage';
import newUserPage from '@/pages/newUserPage';
import userPage from '@/pages/userPage';
import editUserPage from '@/pages/editUserPage';
import editDepartmentPage from '@/pages/editDepartmentPage';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: IndexLayout,
      children: [
        {
          path: '',
          name: 'LoginPage',
          component: LoginPage
        }
      ]
    },
    {
      path: '/dashboard',
      component: DashboardLayout,
      children: [
        {
          path: 'users',
          name: 'UsersPage',
          component: UsersPage
        },
        {
          path: 'tasks',
          component: TasksLayout,
          children: [
            {
              path: '',
              name: 'TasksPage',
              component: TasksPage
            },
            {
              path: 'date/:date',
              name: 'currentTasksPage',
              component: currentTasksPage
            },
            {
              path: 'create',
              name: 'newTaskPage',
              component: newTaskPage
            }
          ]
        },
        {
          path: 'devices',
          name: 'DevicesPage',
          component: DevicesPage
        },
        {
          path: 'tickets',
          name: 'TicketsPage',
          component: TicketsPage
        },
        {
          path: 'newDepartment',
          name: 'newDepartmentPage',
          component: newDepartmentPage
        },
        {
          path: 'newUserPage',
          name: 'newUserPage',
          component: newUserPage
        },
        {
          path: 'editUser/:id',
          name: 'editUserPage',
          component: editUserPage
        },
        {
          path: 'editDepartment/:id',
          name: 'editDepartmentPage',
          component: editDepartmentPage
        },
        {
          path: 'user/:id',
          name: 'userPage',
          component: userPage
        },
      ]
    }
  ]
})


router.beforeEach((to, from, next) => {
  if (to.name === 'LoginPage') {
    Vue.http.get(store.state.api.url+'user')
    .then(res => {
      router.push({ name: 'UsersPage' });
    }, err => {})  
  }
  Vue.http.get(store.state.api.url+'user')
  .then(res => {
    store.commit('SET_USER_DATA', res.body)
    next()
  }, err => {
    console.log(err)
    next()
  })
})

export { router };