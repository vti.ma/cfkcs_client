import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
    	user: '',
        password_default: '4768812cfkcs.ru',
	  	api: {
            root: 'http://192.168.88.247/',
	  		url: 'http://192.168.88.247/api/v1/',
	  		grant_type: 'password',
	  		scopes: '*',
	  		client_id: 3,
			client_secret: 'iNBYL07O4aNvIIXwh7HHOkKVS9vNFdeclAsDRTdb',
	  	}
    },
    mutations: {
        SET_USER_DATA (state, payload) {
            state.user = payload
        }
    }
})

export { store };